package com.java.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;

import com.java.entity.Post;

public interface PostService {

	public List<Post> findAll(PageRequest pageRequest);


}
