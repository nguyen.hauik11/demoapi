package com.java.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.java.entity.Post;
import com.java.entity.User;
import com.java.repository.UserRepository;
import com.java.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	UserRepository userRepository;

	@RequestMapping(value = "/users/{page}/{size}", method = RequestMethod.GET)
	public ResponseEntity<List<User>> findAll(@PathVariable("page") int page, @PathVariable("size") int size) {

		PageRequest pageable = PageRequest.of(page, size);
		List<User> listUser = userService.findByPage(pageable);
		if (listUser.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(listUser, HttpStatus.OK);
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public User saveUser(@Valid @RequestBody User user, BindingResult result) {

		String pass = user.getPassword();
		BCryptPasswordEncoder cryptPasswordEncoder = new BCryptPasswordEncoder();
		String passwords = cryptPasswordEncoder.encode(pass);
		user.setPassword(passwords);
		user.setStatus(true);
		user.setCreated_at(LocalDate.now());
		user.setModified_at(LocalDate.now());

//		String url=fileUpload(user.getAvatar());
		if (result.hasErrors()) {
			return null;
		}
		return userService.register(user);

	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam("username") String username, @RequestParam("password") String password) {

		User user = userService.login(username);
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		boolean pass = passwordEncoder.matches(password, user.getPassword());
		if (pass) {
			return "ok";
		}
		return "fail";

	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.PUT)
	public ResponseEntity<User> updateUser(@PathVariable("id") int id, @Valid @RequestBody User user1) {
		User user = userService.getOne(id);
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		user.setUsername(user1.getUsername());
		String pass = user1.getPassword();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		user.setPassword(passwordEncoder.encode(pass));
		user.setAvatar(user1.getAvatar());
		user.setFullname(user1.getFullname());
		user.setEmail(user1.getEmail());
		user.setPhone(user1.getPhone());
		user.setStatus(true);
		user.setCreated_at(LocalDate.now());
		user.setModified_at(LocalDate.now());
		user.setBirthday(user1.getBirthday());
		user.setToken(user1.getToken());

		User Update = userService.save(user);
		return ResponseEntity.ok(Update);
	}

	@RequestMapping(value = "/users{id}", method = RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable("id") int id) {
		User user = userService.getOne(id);
		if (user == null) {
			return ResponseEntity.notFound().build();
		}
		userService.delete(user);
		return ResponseEntity.ok().build();
	}

	@RequestMapping(value = "/users/upload", method = RequestMethod.POST)
	public String fileUpload(@RequestParam("file") MultipartFile file) throws IOException {
		File file1 = new File("C:\\Users\\NGUYEN\\eclipse-workspace\\Test\\src\\image\\" + file.getOriginalFilename());
//		file1.createNewFile(); chua co file save thi khoi tao
		try {
			FileOutputStream image = new FileOutputStream(file1);
			image.write(file.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		String url = file1.getPath();
		return url;
	}

	@RequestMapping(value = "/userss", method = RequestMethod.POST)
	public User saveUsers(@RequestParam("username") String username, @RequestParam("password") String password,
			@RequestParam(name = "fullname", required = false) String fullname,
			@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "phone", required = false) String phone, @RequestParam("file") MultipartFile file) {

		// dung form thi k can check validator
		File file1 = new File("C:\\Users\\NGUYEN\\eclipse-workspace\\Test\\src\\image\\" + file.getOriginalFilename());
//		file1.createNewFile(); chua co file save thi khoi tao
		try {
			FileOutputStream image = new FileOutputStream(file1);
			image.write(file.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		BCryptPasswordEncoder cryptPasswordEncoder = new BCryptPasswordEncoder();
		String passwords = cryptPasswordEncoder.encode(password);

		User user = new User();
		user.setUsername(username);
		user.setPassword(passwords);
		user.setFullname(fullname);
		user.setStatus(true);
		user.setCreated_at(LocalDate.now());
		user.setModified_at(LocalDate.now());
		user.setAvatar(file1.getPath());
		user.setPhone(phone);
		user.setEmail(email);

		return userService.register(user);
	}

	


}
