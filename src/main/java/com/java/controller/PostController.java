package com.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.java.entity.Post;
import com.java.entity.User;
import com.java.service.PostService;
import com.java.service.UserService;

@RestController
@RequestMapping("/api")
public class PostController {

	@Autowired
	PostService postService;
	@Autowired
	UserService userService;

	@RequestMapping(value = "/posts/{page}/{size}", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> findAll(@PathVariable("page") int page, @PathVariable("size") int size) {

		PageRequest pageRequest = PageRequest.of(page, size);
		List<Post> listPosts = postService.findAll(pageRequest);
		if (listPosts.isEmpty()) {
			return new ResponseEntity<List<Post>>(listPosts, HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Post>>(listPosts, HttpStatus.OK);
	}

	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<Post>> findAllOfUser(@PathVariable("id") int id) {
		User user= userService.getOne(id);
		if(!userService.exist(user)) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		List<Post> listPost= (List<Post>) user.getPosts();
		if(listPost==null) {
			return null;
		}
		return new ResponseEntity<List<Post>>(listPost, HttpStatus.ACCEPTED.OK);
	}
	

}
