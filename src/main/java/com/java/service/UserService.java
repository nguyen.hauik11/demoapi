package com.java.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.PageRequest;

import com.java.entity.User;

public interface UserService {

	public Boolean exist(User user);

	public List<User> findAll();

	public User getOne(int id);

	public User save(@Valid User user);

	public void delete(User user);

	public User register(User user);

	public User login(String username );

	public List<User> findByPage(PageRequest pageRequest);
}
