package com.java.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name = "post")
@Data
@JsonIgnoreProperties({"listComment", "user"})
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String slug;
	private String summary;
	private String content;
	private String image;
	private String meta_title;
	private String meta_keywords;
	private String meta_description;
	private int num_views;
	private Boolean status;
	private LocalDate created_at;
	private LocalDate modified_at;
	@ManyToOne 
    @JoinColumn(name = "user_id") 
    private User user;

	@OneToMany(mappedBy = "post_id")
	private List<Comments> listComment;

	
	

	

}
