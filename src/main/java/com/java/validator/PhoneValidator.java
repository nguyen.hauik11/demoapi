package com.java.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, String>{

	@Override
	public boolean isValid(String phone, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		if(phone==null) {
			return false;
		}
		String sdt="^0\\d{9}";
		boolean matcher= Pattern.matches(sdt, phone);
		return matcher;
	}

}
