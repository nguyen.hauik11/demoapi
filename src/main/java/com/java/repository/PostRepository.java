package com.java.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.java.entity.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {

	Page<Post> findAll(Pageable pageable);

}
