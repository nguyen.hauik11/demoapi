package com.java.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.java.entity.Post;
import com.java.repository.PostRepository;
import com.java.repository.UserRepository;
import com.java.service.PostService;

@Configuration
public class PostServiceImpl implements PostService {

	@Autowired
	PostRepository postRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public List<Post> findAll(PageRequest pageRequest) {
		// TODO Auto-generated method stub
		Page<Post> listPage = postRepository.findAll(pageRequest);
		return listPage.getContent();
	}


}
