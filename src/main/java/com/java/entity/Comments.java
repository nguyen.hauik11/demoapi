package com.java.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="comments")
@Getter
@Setter
public class Comments {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int parent_id;
	@ManyToOne
	@JoinColumn(name="post_id")
	private Post post_id;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user_id;
	
	private String content;
	private Boolean status;
	
	private LocalDate created_at;
	private LocalDate modified_at;
	
	

}
