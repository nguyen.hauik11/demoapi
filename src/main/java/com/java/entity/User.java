package com.java.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.java.validator.Email;
import com.java.validator.Phone;

import lombok.Data;

@Entity
@Table(name = "users")
@Data
@JsonIgnoreProperties({"listComments", "posts"})
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Size(min = 6, max = 30)
	private String username;
	private String password;
	@Size(min = 6, max = 30)
	private String fullname;

	@Email
	private String email;

	@Phone
	private String phone;
	private String avatar;
	private Date birthday;
	private boolean status;
	private LocalDate created_at;
	private LocalDate modified_at;
	private String token;
	@OneToMany(mappedBy = "user") 
	private Collection<Post> posts;

	@OneToMany(mappedBy = "user_id")
	private List<Comments> listComments;

	
}
