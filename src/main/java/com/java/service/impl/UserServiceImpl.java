package com.java.service.impl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.java.entity.CustomUserDetails;
import com.java.entity.User;
import com.java.repository.UserRepository;
import com.java.service.UserService;

@Configuration
public class UserServiceImpl implements UserService, UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Boolean exist(User user) {
		// TODO Auto-generated method stub
		User user1 = userRepository.findByUsername(user.getUsername());
		if (user1 == null) {
			return false;
		}
		return true;
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User save(@Valid User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		userRepository.delete(user);

	}

	@Override
	public User register(User user) {
		// TODO Auto-generated method stub
		if (!exist(user)) {
			return save(user);
		}
		return null;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			return (UserDetails) new UsernameNotFoundException(username);
		}
		return new CustomUserDetails(user);
	}

	@Override
	public User login(String username) {
		// TODO Auto-generated method stub
		User user= userRepository.findByUsername(username);
		if(!exist(user)) {
			return null;
		}
		return user;
	}

	@Override
	public List<User> findByPage(PageRequest pageRequest) {
		Page<User> Page = userRepository.findAll(pageRequest);
	    return Page.getContent();
	}

	@Override
	public User getOne(int id) {
		// TODO Auto-generated method stub
		return userRepository.getOne(id);
	}

	

}
